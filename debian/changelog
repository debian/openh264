openh264 (2.6.0+dfsg-2) unstable; urgency=medium

  * Remove libopenh264.so.8 library from the right path

 -- Bastian Germann <bage@debian.org>  Thu, 27 Feb 2025 19:58:32 +0100

openh264 (2.6.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.6.0+dfsg (Closes: #1098470, CVE-2025-27091)
  * Fix SHA256 checksums for 2.6.0
  * Rename binary pkgs because of SO bump

 -- Bastian Germann <bage@debian.org>  Fri, 21 Feb 2025 08:30:38 +0100

openh264 (2.5.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.5.0+dfsg
  * Add new copyright statement
  * Fix SHA256 checksums for 2.5.0

 -- Bastian Germann <bage@debian.org>  Mon, 11 Nov 2024 08:44:22 +0100

openh264 (2.4.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2.4.1+dfsg
  * Replace wget and bzip2 with apt-helper
  * Fix SHA256 checksums for 2.4.1

 -- Bastian Germann <bage@debian.org>  Thu, 15 Feb 2024 20:40:04 +0000

openh264 (2.4.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.4.0+dfsg
  * d/copyright: Introduce wildcards to cover new files
  * Fix SHA256 checksums for 2.4.0
  * Drop upstream patch

 -- Bastian Germann <bage@debian.org>  Wed, 29 Nov 2023 17:02:28 +0100

openh264 (2.3.1+dfsg-4) unstable; urgency=medium

  * Move to Multimedia Maintainers

 -- Bastian Germann <bage@debian.org>  Tue, 19 Sep 2023 13:14:18 +0200

openh264 (2.3.1+dfsg-3) unstable; urgency=medium

  * d/rules: Replace DEB_HOST_GNU_CPU with DEB_HOST_ARCH (Closes: #1013097)
  * Patch: Prevent building armel with NEON

 -- Bastian Germann <bage@debian.org>  Mon, 28 Nov 2022 19:01:59 +0100

openh264 (2.3.1+dfsg-2) unstable; urgency=medium

  * Fix SHA256 checksums for 2.3.1

 -- Bastian Germann <bage@debian.org>  Thu, 29 Sep 2022 11:31:18 +0200

openh264 (2.3.1+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields.
  * Update standards version to 4.6.0, no changes needed.

  [ Bastian Germann ]
  * d/watch: Switch to GitHub tags (releases page changed)
  * Move to SO version 7
  * New upstream version 2.3.1+dfsg

 -- Bastian Germann <bage@debian.org>  Thu, 29 Sep 2022 11:02:42 +0200

openh264 (2.3.0+dfsg-2) unstable; urgency=medium

  * Fix SHA256 checksums for 2.3.0

 -- Bastian Germann <bage@debian.org>  Mon, 29 Aug 2022 16:07:48 +0200

openh264 (2.3.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.3.0+dfsg
  * d/copyright: Add info from new upstream version

 -- Bastian Germann <bage@debian.org>  Mon, 15 Aug 2022 22:46:57 +0200

openh264 (2.2.0+dfsg-3) unstable; urgency=medium

  [ Laurent Bigonville ]
  * Patch: Add x32 build flavour (Closes: #1013097)

 -- Bastian Germann <bage@debian.org>  Fri, 17 Jun 2022 09:20:50 +0200

openh264 (2.2.0+dfsg-2) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Also supply build tools to make install (Closes: #1006305)

 -- Bastian Germann <bage@debian.org>  Wed, 23 Feb 2022 07:15:26 +0100

openh264 (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.0+dfsg
  * Update Cisco binaries' SHA256 sums

 -- Bastian Germann <bage@debian.org>  Fri, 04 Feb 2022 15:50:23 +0100

openh264 (2.1.1+dfsg-2) unstable; urgency=medium

  * Cut out the +dfsg for download URL

 -- Bastian Germann <bage@debian.org>  Tue, 18 Jan 2022 20:24:42 +0100

openh264 (2.1.1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #974678)

 -- Bastian Germann <bage@debian.org>  Thu, 13 May 2021 13:40:56 +0200
